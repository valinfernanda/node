//https://nodejs.org/dist/latest-v14.x/docs/api/fs.html#fs_file_system

const fs = require('fs');

// console.log(fs)
// menuliskan string ke file (synchronous)
// fs.writeFileSync('test.txt', 'Hello World secara async')
//kalo test.txt gaada di folder yang sama, maka akan dibuatkan secara otomatis 

//menuliskan string ke file (async)
//https://nodejs.org/dist/latest-v14.x/docs/api/fs.html#fs_fs_writefile_file_data_options_callback
// fs.writeFile('message.txt', 'Hello', (e) => {
//     console.log(e);
//   });

//membaca isi file (synchronous)
// const data = fs.readFileSync('./test.txt', 'utf-8')
// console.log(data.toString()) -> ini kalo gapake utf-8
// console.log(data)

//membaca isi file (async)
// fs.readFile('test.txt', 'utf-8', (err, data)=> {
//     if(err) throw err; 
//     console.log(data)
// })

//readline
//https://nodejs.org/dist/latest-v14.x/docs/api/readline.html
const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.question('Masukkan nama anda: ', (nama) => {
rl.question('Masukkan nomor HP anda: ', (nomor) => {
    const contact = {
        nama: nama, 
        noHP: nomor,
    }
    const file= fs.readFileSync('./contacts.json', 'utf-8')
    const contacts = JSON.parse(file);
    
    contacts.push(contact)
    console.log(contacts)

    fs.writeFileSync('./contacts.json', JSON.stringify(contacts))

    
    console.log(`Terimakasih sudah memasukkan data` )
    rl.close()
})

})

