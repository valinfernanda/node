function cetakNama(nama) {
    return `Hello, apakabar ${nama}`;
}

const PI = 3.14;

const mahasiswa = {
    nama : "Valin Fernanda", 
    umur : 24,
    cetakMhs(){
        return `Hallo, nama saya ${this.nama}, umur saya ${this.umur}`
    }
}

class Orang {
    constructor(){
        console.log('Objek orang telah dibuat')
    }
}

// module.exports.cetakNama = cetakNama;
// module.exports.PI = PI;
// module.exports.mahasiswa=mahasiswa;
// module.exports.Orang=Orang;
module.exports = {cetakNama, PI, mahasiswa, Orang}