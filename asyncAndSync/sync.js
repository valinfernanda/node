const getUserSync = (id) => {
  //   let nama = "";
  //   if (id === 1) {
  //     nama = "Valin";
  //   } else {
  //     nama = "Laura";
  //   }
  const nama = id === 1 ? "Valin" : "Laura";
  return { id, nama };
};

const UserSatu = getUserSync(1);
console.log(UserSatu);
const UserDua = getUserSync(2);
console.log(UserDua);
