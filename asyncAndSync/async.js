const getUserAsync = (id, cb) => {
  const time = id === 1 ? 3000 : 2000;
  setTimeout(() => {
    const nama = id === 1 ? "Valin" : "Laura";
    cb({ id, nama });
  }, time);
};

const userSatu = getUserAsync(1, (hasil) => {
  console.log(hasil);
});

const userDua = getUserAsync(2, (hasil) => {
  console.log(hasil);
});

const hallo = "Yuhu";
console.log(hallo);
